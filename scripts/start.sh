#!/bin/sh
set -x

source .env
npx prisma migrate deploy --schema /calcom/packages/prisma/schema.prisma
yarn start
